import random
from math import log2, exp, gamma
from scipy.stats import chi2

class LeafNode:
    def __init__(self, value, parent_attribute_value=None):
        self.value = value
        self.parent_attribute_value = parent_attribute_value or ("", "")
    
    def __call__(self, example):
        return self.value

    def __repr__(self):
        return f"Leaf(ParentAttribute=({self.parent_attribute_value[0]}, {self.parent_attribute_value[1]}), Value={self.value})"

    def print(self, level=0):
        print(f"Lvl {level}, {str(self)}")

class DecisionNode:
    def __init__(self, attribute, parent_attribute_value = None):
        """ construct a decision node for given attribute """
        self.children = {}
        self.attribute = attribute
        self.parent_attribute_value = parent_attribute_value or ("", "")

    def __call__(self, example):
        """ Classify given example based on given decision tree """
        # start at the root node
        if example[self.attribute] in self.children:
            return self.children[example[self.attribute]](example)
        else:
            raise ValueError("Decision attribute not present.")

    def add_branch(self, value, subtree):
        """ adds a test for given value and the linked subtree """
        subtree.parent_attribute_value = (self.attribute, value)
        self.children[value] = subtree
    
    def __repr__(self):
        return f"DecisonNode(ParentAttribute=({self.parent_attribute_value[0]}, {self.parent_attribute_value[1]}), Attribute={self.attribute})"
        
    def print(self, level=0):
        print(f"Lvl {level}, {str(self)}")
        for val, child in self.children.items():
            child.print(level=level + 1)

    def has_leaves_only(self):
        for value, child in self.children.items():
            if not isinstance(child, LeafNode):
                return False
        return True

    def child_decision_nodes(self):
        child_decision_nodes = []
        for child in self.children:
            if isinstance(child, DecisionNode):
                child_decision_nodes.append(child)
        return child_decision_nodes


def count_values(val_list):
    """ Count the occuring values in given list """
    values_counts = {}
    for v in val_list:
        if v in values_counts.keys():
            values_counts[v] += 1
        else:
            values_counts[v] = 1
    return values_counts


def entropy(probs):
    """ Calculate entropy of given discrete probability distribution """
    return sum(-p*log2(p) for p in probs)


def attribute_entropy(attribute, examples):
    """ Calculate entropy of given attribute on given examples """
    num_examples = len(examples)
    values = [examples[i][attribute] for i in range(num_examples)]
    values_count = count_values(values)
    probs = [val_count/num_examples for val_count in values_count.values()]
    assert sum(probs) == 1.
    return entropy(probs)


def importance(attribute, examples, output_attribute):
    """ Calculate importance of given attribute for given examples """
    num_examples = len(examples)
    # calculate entropy of output_attribute (TODO precompute)
    entropy_output = attribute_entropy(output_attribute, examples)
    values = [examples[i][attribute] for i in range(len(examples))]

    # split examples into subsets of different attribute values
    examples_values = {}
    for ex in examples:
        value = ex[attribute]
        if value in examples_values:
            examples_values[value].append(ex)
        else:
            examples_values[value] = [ex]

    # calculate remaining entropy after splitting on attribute
    entropy_remainder = 0
    for value, examples_subset in examples_values.items():
        subset_entropy = attribute_entropy(output_attribute, examples_subset)
        entropy_remainder += len(examples_subset) * subset_entropy
    entropy_remainder /= num_examples

    return entropy_output - entropy_remainder


def plurality_value(examples, attribute):
    """ Determine plurality value  of given attribute in list of examples """
    # count the number of examples with occuring values
    output_values_counts = {}
    for e in examples:
        if e[attribute] in output_values_counts.keys():
            output_values_counts[e[attribute]] += 1
        else:
            output_values_counts[e[attribute]] = 0
    # determine the plurality value
    max_count = 0
    max_values = []
    for val, count in output_values_counts.items():
        if count == max_count:
            max_values.append(val)
        elif count > max_count:
            max_values = [val]
            max_count = count
    if len(max_values) > 1: # break ties randomly
        max_value_idx = random.randint(0, len(max_values) - 1)
    else:
        max_value_idx = 0
    return max_values[max_value_idx]
    

def have_same_attribute_values(examples, attribute):
    """ Check whether given examples all have same attribute value """
    if len(examples) == 0:
        return False
    value = examples[0][attribute]
    for e in examples[1:]:
        if e[attribute] != value:
            return False
    return True


def argmax(values):
    """ Return deterministic argmax of given list of values """
    max_val = 0
    max_idx = -1
    for i, val in enumerate(values):
        if val > max_val:
            max_val = val
            max_idx = i
    return max_idx


def learn_decision_tree(examples, attributes, attribute_values, parent_examples, output_attribute):
    """ Learn a decision tree from given examples, for given attributes """
    if len(examples) == 0: # when no examples left, classify according to the plurality in parent examples
        return LeafNode(plurality_value(parent_examples, output_attribute))
    elif have_same_attribute_values(examples, output_attribute): # when all classifications equal we are done
        return LeafNode(examples[0][output_attribute])
    elif len(attributes) == 0: # when no attributes are left, classify according to plurality in examples
        return LeafNode(plurality_value(examples, output_attribute))
    else:
        # calculate information gain for every attribute
        importances = [importance(a, examples, output_attribute) for a in attributes]
        # get best attribute to split on
        best_attribute_index = argmax(importances)
        best_attribute = attributes[best_attribute_index]
        tree = DecisionNode(best_attribute)
        # remove attribute from available ones
        new_attributes = attributes.copy()
        new_attributes.pop(attributes.index(best_attribute))
        # iterate through all possible attribute values
        for value in attribute_values[best_attribute]:
            # filter out examples that have this attribute value
            examples_with_value = []
            for ex in examples:
                if ex[best_attribute] == value:
                    examples_with_value.append(ex)
            # learn a decision tree with the examples that satisfy the condition
            subtree = learn_decision_tree(examples_with_value, new_attributes, attribute_values, examples, output_attribute)
            # add the subtree
            tree.add_branch(value, subtree)
        return tree


def generate_random_examples(num_examples, attribute_values, base_tree=None, output_attribute=None):
    """ Generate random examples from given attribute values (and optimal labels from base decision tree) """
    examples = []
    for i in range(num_examples):
        ex = {}
        for attr, values in attribute_values.items():
            ex[attr] = random.choice(values)
        if base_tree is not None and output_attribute is not None:
            ex[output_attribute] = base_tree(ex)#classify(ex, base_tree)
        examples.append(ex)
    return examples


def test_decision_tree(examples, decision_tree, output_attribute):
    """ Returns the accuracy of a decision tree on the given examples """
    num_correct = 0
    for ex in examples:
        pred = decision_tree(ex)
        label = ex[output_attribute]
        if pred == label:
            num_correct += 1
    return num_correct/len(examples)


def construct_example_decision_tree():
    """ Construct the example decision tree from AIMA (4th edition), figure 19.3 """

    frisat = DecisionNode("Fri")
    frisat.add_branch("No", LeafNode("No"))
    frisat.add_branch("Yes", LeafNode("Yes"))

    bar = DecisionNode("Bar")
    bar.add_branch("No", LeafNode("No"))
    bar.add_branch("Yes", LeafNode("Yes"))

    raining = DecisionNode("Rain")
    raining.add_branch("No", LeafNode("No"))
    raining.add_branch("Yes", LeafNode("Yes"))

    alternate2 = DecisionNode("Alt")
    alternate2.add_branch("No", LeafNode("Yes"))
    alternate2.add_branch("Yes", raining)

    hungry = DecisionNode("Hun")
    hungry.add_branch("No", LeafNode("Yes"))
    hungry.add_branch("Yes", alternate2)

    reserved = DecisionNode("Res")
    reserved.add_branch("No", bar)
    reserved.add_branch("Yes", LeafNode("Yes"))

    alternate = DecisionNode("Alt")
    alternate.add_branch("No", reserved)
    alternate.add_branch("Yes", frisat)

    waitestimate = DecisionNode("Est")
    waitestimate.add_branch(">60", LeafNode("No"))
    waitestimate.add_branch("30-60", alternate)
    waitestimate.add_branch("10-30", hungry)
    waitestimate.add_branch("0-10", LeafNode("Yes"))

    patrons = DecisionNode("Pat")
    patrons.add_branch("None", LeafNode("No"))
    patrons.add_branch("Some", LeafNode("Yes"))
    patrons.add_branch("Full", waitestimate)
    return patrons

def count_attribute_values(examples, attribute, attribute_values):
    attribute_value_count = {v: 0 for v in attribute_values[attribute]}
    for value in attribute_values[attribute]:
        for ex in examples:
            if ex[attribute] == value:
                attribute_value_count[value] += 1
    return attribute_value_count

def split(examples, attribute):
    examples_values = {}
    for ex in examples:
        value = ex[attribute]
        if value in examples_values:
            examples_values[value].append(ex)
        else:
            examples_values[value] = [ex]
    return examples_values

def chi_squared_pdf(x, k):
    if x > 0:
        return (x ** (k / 2 - 1) * exp(-x/2))/(2 ** (k / 2) * gamma(k / 2))
    else:
        return 0

def chi_square_pruning(examples, output_attribute, attribute_values, decision_node, significance=0.05):
    """ Apply chi-squared pruning to a given decision tree """

    if not decision_node.has_leaves_only():
        has_pruned = False
        examples_value = split(examples, decision_node.attribute)
        # recursively apply chi-square pruning to all children
        for value, child in decision_node.children.items():
            if isinstance(child, DecisionNode) and value in examples_value:
                node_pruned, pruned_node = chi_square_pruning(examples_value[value], output_attribute, attribute_values, child, significance)
                has_pruned = node_pruned or has_pruned
                decision_node.children[value] = pruned_node or child
        if has_pruned:
            # if child decision nodes have been pruned, try to prune parent as well
            return chi_square_pruning(examples, output_attribute, attribute_values, decision_node, significance)
        else:
            return False, decision_node

    else: # this decision node has only leafs
        attribute = decision_node.attribute
        # count values for dataset
        total_output_value_counts = count_attribute_values(examples, output_attribute, attribute_values)

        # lists of lists (outer list = split attribute values, inner list = output attribute valuess)
        measured_counts = []
        expected_counts = []

        # count values for subsets
        for value, ex_subset in split(examples, attribute).items():
            subset_value_counts = count_attribute_values(ex_subset, output_attribute, attribute_values)
            exp_out_counts = [] 
            meas_out_counts = []
            for out_value in attribute_values[output_attribute]:
                expected_val = total_output_value_counts[out_value]*(len(ex_subset)/len(examples))
                exp_out_counts.append(expected_val)
                meas_out_counts.append(subset_value_counts[out_value])
            measured_counts.append(meas_out_counts)
            expected_counts.append(exp_out_counts)

        # calculate chi-squared statistic
        delta = 0
        for val_idx in range(len(measured_counts)):
            for out_val_idx in range(len(measured_counts[0])):
                try:
                    delta += (measured_counts[val_idx][out_val_idx] - expected_counts[val_idx][out_val_idx]) ** 2 / expected_counts[val_idx][out_val_idx]
                except ZeroDivisionError:
                    pass
        
        # probability of delta
        dof = len(measured_counts) - 1
        # high p_dev = low deviation = attribute irrelevant
        p_deviation = 1 - chi2.cdf(delta, dof) # chi_squared_pdf(delta, dof)

        if p_deviation > significance:
            # if the calculated deviation is likely (at given significance) accept H0 and prune
            #print(f"Irrelevant attribute since p_dev={p_deviation} > {significance}")
            return True, LeafNode(plurality_value(examples, attribute))
        else:
            #print(f"Relevant attribute since p_dev={p_deviation} < {significance}")
            return False, None
        

if __name__ == "__main__":
    import csv

    # test decision tree implementation

    # define attribute names and possible values
    attribute_values = {"Alt": ["Yes", "No"],
                        "Bar": ["Yes", "No"],
                        "Fri": ["Yes", "No"],
                        "Hun": ["Yes", "No"],
                        "Pat": ["None", "Some", "Full"],
                        "Price": ["$", "$$", "$$$"],
                        "Rain": ["Yes", "No"],
                        "Res": ["Yes", "No"],
                        "Type": ["French", "Thai", "Italian", "Burger"],
                        "Est": ["0-10", "10-30", "30-60", ">60"],
                        "WillWait": ["Yes", "No"]}
    attributes = list(attribute_values.keys())

    # load examples
    examples = []
    with open("restaurant.csv", newline="") as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=attributes, delimiter=",")
        for row in reader:
            row = {k: v.strip() for k, v in row.items()}
            examples.append(row)

    # learn decision tree
    input_attributes = attributes
    output_attribute = "WillWait"
    input_attributes.pop(input_attributes.index(output_attribute))
    decision_tree = learn_decision_tree(examples, input_attributes, attribute_values, [], output_attribute)

    # check whether importance function works
    for ia in input_attributes:
        print(f"Importance({ia}) = ", importance(ia, examples, output_attribute))
    
    # predict on examples from training set
    print("Accuracy on training set = ", test_decision_tree(examples, decision_tree, output_attribute))

    # visualize tree
    decision_tree.print()

    # generate random dataset based on example tree
    num_examples = 100
    example_tree = construct_example_decision_tree()
    dataset = generate_random_examples(num_examples, attribute_values, example_tree, output_attribute)

    # chi-squared decision tree pruning
    print("Example tree")
    example_tree.print()
    print("Pruned tree")
    was_pruned, pruned_decision_tree = chi_square_pruning(dataset, output_attribute, attribute_values, example_tree)
    pruned_decision_tree.print()   

    # reproduce learning curve (fig 19.7 AIMA)
    num_trials = 20
    mean_accuracies = []
    for i in range(1, num_examples):
        accuracies = []
        for trials in range(num_trials):
            random.shuffle(dataset)
            train_set = dataset[:i]
            test_set = dataset[i:]
            decision_tree2 = learn_decision_tree(train_set, input_attributes, attribute_values, [], output_attribute)
            accuracy = test_decision_tree(test_set, decision_tree2, output_attribute)
            accuracies.append(accuracy)
        mean_accuracy = sum(accuracies)/num_trials
        mean_accuracies.append(mean_accuracy)
        print(f"iteration {i}, accuracy {mean_accuracy:0.4f}")
    
    import matplotlib.pyplot as plt
    plt.plot(mean_accuracies)
    plt.xlabel("training test size")
    plt.ylabel("accuracy")
    plt.show()